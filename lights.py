#!/usr/bin/python

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import RPi.GPIO as GPIO
from astral import Astral
from datetime import datetime
import time
import sys

# get pin from config :)
from config import LIGHTING_PIN as pin

#BCM
#pin = 24
GPIO.setmode(GPIO.BCM)

class RPiObj(object):
    pin = None
    name = ""

class RPiOut(RPiObj):
    def __init__(self, pin, name=None, initial=GPIO.LOW):
        self.pin = pin
        self.name = (name if name is not None else "OUT Pin %s" % pin)
        GPIO.setup(pin, GPIO.OUT, initial=initial)

class RPiRelay(RPiOut):    
    def on(self):
        GPIO.setup(self.pin, GPIO.OUT)
        GPIO.output(self.pin, GPIO.HIGH)
        
    def off(self):
        #GPIO.output(self.pin, GPIO.LOW)
        #somehow LOW isn't enough for this one...
        GPIO.cleanup(self.pin)

class RpiLightingSunrise(RPiRelay):
    city = None
    
    def __init__(self, pin, location="Amsterdam"):
        self.pin = pin
        self.location = location
        GPIO.setup(pin, GPIO.OUT)
        self.updateTimes()
    
    def updateTimes(self):
        astral = Astral()
        astral.solar_depression = 'civil'
        self.city = astral[self.location]
    
    def _getSun(self):
        return self.city.sun(local=True)
    
    def getSunset(self):
        return self._getSun()['sunset']
    
    def getSunrise(self):
        return self._getSun()['sunrise']
    
    def run(self, update=True):
        if update:
            self.updateTimes()

        now = datetime.now()
        sunrise = self.getSunrise().replace(tzinfo=None)
        sunset = self.getSunset().replace(tzinfo=None)
        if now > sunrise and now < sunset:
            self.on()
        else:
            self.off()

if __name__ == '__main__':
    GPIO.setwarnings(False)
    print "using pin %s" % pin
    lights = RpiLightingSunrise(pin)
    try:
        while 1:
            lights.run()
            time.sleep(5)
    except KeyboardInterrupt:
        sys.exit(0)
