"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import RPi.GPIO as GPIO
from astral import Astral
from datetime import datetime
import os

class RPiException(Exception):
    """ BaseException for RPi """
    pass

class RPiArgumentException(RPiException):
    """ ArgumentException for RPi """
    pass

class RPiValueException(RPiException):
    """ ValueException for RPi """
    pass

class Temperature:
    def __init__(self, rawData):
        self.rawData = rawData
    @property
    def C(self):
        return float(self.rawData) / 1000
    @property
    def F(self):
        return self.C * 9.0 / 5.0 + 23.0

class WaterFlowSensor:
    q = 7.5
    freq = 60
    NbTopsFan = 0
    
    @property
    def liters_per_hour(self):
        return self.liters_per_minute() * 60
    
    @property
    def liters_per_minute(self):
        return ((self.NbTopsFan / self.q) / self.freq)
    
    @property
    def liters_per_second(self):
        return self.liters_per_minute() / 60

class YFS201(WaterFlowSensor):    
    pass

class RPiObj(object):
    pin = None
    name = ""

class RPiIn(RPiObj):
    def __init__(self, pin, name=None):
        self.pin = pin
        self.name = (name if name is not None else "IN Pin %s" % pin)
        GPIO.setup(pin, GPIO.IN)

class RPiOut(RPiObj):
    def __init__(self, pin, name=None, initial=GPIO.LOW):
        self.pin = pin
        self.name = (name if name is not None else "OUT Pin %s" % pin)
        GPIO.setup(pin, GPIO.OUT, initial=initial)

class RPiHAL(RPiIn):
    def __init__(self, pin, name=None):
        self.pin = pin
        self.name = (name if name is not None else "HAL Pin %s" % pin)
        GPIO.setup(pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)

class RPiRelay(RPiOut):
    state = None
    
    def on(self):
        GPIO.setup(self.pin, GPIO.OUT)
        GPIO.output(self.pin, GPIO.HIGH)
        self.state = True
        
    def off(self):
        #GPIO.output(self.pin, GPIO.LOW)
        #somehow LOW isn't enough for this one...
        GPIO.cleanup(self.pin)
        self.state = False
        

        
class RPiRelayBoard(object):
    name = "RpIRelayBoard"
    relays = {}
    
    def __init__(self, name=None):
        if name is not None:
            self.name = name
    
    def addRelay(self, pin, name=None, initial=GPIO.LOW):
        self.relays[pin] = RPiRelay(pin, name, initial)
    
    def addRelayObject(self, RpiObj):
        if not isinstance(RpiObj, RPiRelay):
            raise RPiArgumentException("Invalid object")
        self.relays[RpiObj] = RpiObj
    
    def list(self):
        return self.relays
    
    def getRelay(self, pin):
        return self.relays[pin]


class RPiLightingAstral(RPiRelay):
    city = None
    
    def __init__(self, pin, name, location="Amsterdam"):
        super(RPiLightingAstral, self).__init__(pin, name)
        self.location = location
        self.updateTimes()
    
    def updateTimes(self):
        astral = Astral()
        astral.solar_depression = 'civil'
        self.city = astral[self.location]
    
    def _getSun(self):
        return self.city.sun(local=True)
    
    def getSunset(self):
        return self._getSun()['sunset']
    
    def getSunrise(self):
        return self._getSun()['sunrise']
    
    def run(self, update=True):
        if update:
            self.updateTimes()

        now = datetime.now()
        weekday = now.weekday()
        sunrise = self.getSunrise().replace(tzinfo=None)
        sunset = self.getSunset().replace(tzinfo=None)
        if now > sunrise and now < sunset:
            if weekday > 4:
                if now > now.replace(hour=9, minute=30, second=0, microsecond=0):
                    self.on()
            else:
                if now > now.replace(hour=7, minute=30, second=0, microsecond=0):
                    self.on()
        else:
            self.off()

class RPiDS18B20(RPiIn):
    temperature = Temperature(0)
    def __init__(self, pin, name=None):
        super(RPiDS18B20, self).__init__(pin, name)
        self.tempSensorFile = "/sys/bus/w1/devices/" + pin + "/w1_slave"
        if not os.path.exists(self.tempSensorFile):
            raise RPiArgumentException("Path /sys/bus/w1/devices/" + pin + "/w1_slave does not excist");

    def readFile(self):
        sensorFile = open(self.tempSensorFile, "r")
        lines = sensorFile.readlines()
        sensorFile.close()
        return lines

    def updateTemp(self):
        data = self.readFile()
        self.updateSuccess = False
        if data[0].strip()[-3:] != "YES":
            raise RPiValueException("Invalid reading")
        
        equals_pos = data[1].find("t=")
        if equals_pos == -1:
            raise RPiValueException("Invalid reading")
        
        tempData = data[1][equals_pos+2:]
        self.temperature = Temperature(tempData)
        
        # should be between -55 and +125, poor fish...
        if not (self.temperature.C >= -55 and self.temperature.C <= 125): 
            raise RPiValueException("Invalid temp")
        return self.temperature
                
    def run(self):
        try:
            return self.updateTemp().C
        except RPiValueException as e:
            print(e)


class RPiYFS201(RPiHAL):
    def read(self):
        sensor = YFS201()
        start = datetime.now()
        while True:
            GPIO.wait_for_edge(self.pin, GPIO.RISING)
            if (datetime.now().second - start.second) <= 1:
                sensor.NbTopsFan += 1
            else:
                return sensor
                
    def run(self):
        return self.read()

