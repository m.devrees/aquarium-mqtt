#!/usr/bin/python

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import RPi.GPIO as GPIO
from astral import Astral
from datetime import datetime
import time
import sys

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

# get pin from config :)
from config import AIR_PIN, FILTER_PIN, HEATER_PIN, LIGHTING_PIN

GPIO.setup(AIR_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(FILTER_PIN, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(HEATER_PIN, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(LIGHTING_PIN, GPIO.OUT, initial=GPIO.LOW)

GPIO.output(AIR_PIN, False)
GPIO.output(FILTER_PIN, False)
GPIO.output(HEATER_PIN, False)
GPIO.output(LIGHTING_PIN, False)

GPIO.output(AIR_PIN, GPIO.LOW)
GPIO.output(FILTER_PIN, GPIO.LOW)
GPIO.output(HEATER_PIN, GPIO.LOW)
GPIO.output(LIGHTING_PIN, GPIO.LOW)

GPIO.cleanup()
