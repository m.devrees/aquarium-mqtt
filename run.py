#!/usr/bin/python

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


# get pin from config :)
from config import AIR_PIN, FILTER_PIN, HEATER_PIN, LIGHTING_PIN 
from config import MQTT_ENABLED, MQTT_HOST, MQTT_PORT, MQTT_USER, MQTT_PASS
from myrpi import RPiLightingAstral, RPiRelayBoard, RPiException
import RPi.GPIO as GPIO
import time
import sys

if __name__ == '__main__':
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    
    import paho.mqtt.client as mqtt
    mqttc = mqtt.Client()
    mqttc.username_pw_set(MQTT_USER, MQTT_PASS)
    mqttc.connect(MQTT_HOST, MQTT_PORT, 60)
    mqttc.loop_start()
    
    print("Setup RelayBoard")
    relayboard = RPiRelayBoard(name="4 ports hub")
    
    print("Setup air")
    relayboard.addRelay(AIR_PIN, name="Air", initial=GPIO.LOW)    
    mqttc.publish("/mycha/home/bedroom/aquarium/air", "0", qos=0)

    print("Setup filter")
    relayboard.addRelay(FILTER_PIN, name="Filter", initial=GPIO.HIGH)
    mqttc.publish("/mycha/home/bedroom/aquarium/filter", "1", qos=0)
    
    print("Setup water heater")
    relayboard.addRelay(HEATER_PIN, name="Water heater", initial=GPIO.HIGH)
    mqttc.publish("/mycha/home/bedroom/aquarium/heater", "1", qos=0)
    
    print("Setup lights")
    lights = RPiLightingAstral(LIGHTING_PIN, "Lights", "Amsterdam")
    print("Sunrise is at %s" % lights.getSunrise())
    print("Sunset is at %s" % lights.getSunset())
    
    relayboard.addRelayObject(lights)
    
    # just in case
    relayboard.getRelay(AIR_PIN).off()

    while True:
        state = None
        try:
            lights.run()
            _state = lights.state
            if state is None or state != _state:
                state = _state
                mqttc.publish("/mycha/home/bedroom/aquarium/lights", "1" if state else "0", qos=0)
            time.sleep(300)
        except KeyboardInterrupt as e:
            print("Cleaning up, air, filter and heater")
            mqttc.publish("/mycha/home/bedroom/aquarium/lights", "0", qos=0)
            mqttc.publish("/mycha/home/bedroom/aquarium/filter", "0", qos=0)
            mqttc.publish("/mycha/home/bedroom/aquarium/heater", "0", qos=0)
            mqttc.publish("/mycha/home/bedroom/aquarium/air", "0", qos=0)
            mqttc.loop_stop()
            GPIO.cleanup([AIR_PIN, FILTER_PIN, HEATER_PIN, LIGHTING_PIN])
            sys.exit(0)
        except RPiException as e:
            print(e)
