#!/usr/bin/python
# name / GPIO	  # relay	# pin
AIR_PIN      =  4 # 1 		#  7
FILTER_PIN   = 17 # 2 		# 11
HEATER_PIN   = 18 # 3 		# 12
LIGHTING_PIN = 22 # 4 		# 13 

MQTT_ENABLED = True
MQTT_HOST = "mqtt.0x43.nl"
MQTT_PORT = 1883
MQTT_USER = ""
MQTT_PASS = ""
