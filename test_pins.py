#!/usr/bin/python

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import RPi.GPIO as GPIO
from astral import Astral
from datetime import datetime
import time
import sys

# get pin from config :)
#from config import LIGHTING_PIN as pin

#BCM
pin = 4 #air
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin, GPIO.OUT)
GPIO.output(pin, True)
time.sleep(5)
GPIO.cleanup(pin)
time.sleep(5)
GPIO.setup(pin, GPIO.OUT)
time.sleep(5)
GPIO.output(pin, True)
time.sleep(5)
GPIO.cleanup(pin)

pin = 17 #filter
GPIO.setup(pin, GPIO.OUT)
GPIO.output(pin, True)
time.sleep(5)
GPIO.cleanup(pin)
time.sleep(5)
GPIO.setup(pin, GPIO.OUT)
time.sleep(5)
GPIO.output(pin, True)
time.sleep(5)
GPIO.cleanup(pin)
time.sleep(5)


pin = 18 #heater
GPIO.setup(pin, GPIO.OUT)
GPIO.output(pin, True)
time.sleep(5)
GPIO.cleanup(pin)
time.sleep(5)
GPIO.setup(pin, GPIO.OUT)
time.sleep(5)
GPIO.output(pin, True)
time.sleep(5)
GPIO.cleanup(pin)
time.sleep(5)

pin = 27 #lights
GPIO.setup(pin, GPIO.OUT)
GPIO.output(pin, True)
time.sleep(5)
GPIO.cleanup(pin)
GPIO.setup(pin, GPIO.OUT)
time.sleep(5)
GPIO.output(pin, True)
time.sleep(5)
GPIO.output(pin, False)

GPIO.cleanup()
